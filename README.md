### CMS DQM Querying Tool
A web-tool for querying CERN's CMS Data Quality Monitoring System, originally built by [Nick Hurley](https://github.com/nickh2000/EMTF-DQM-Website).


![Query Page](readme-media/search-page.png)

![Results 1](readme-media/results-plots.png)

![Results 2](readme-media/results-table.png)

# Transfer ownership and run the website yourself!

Since you're reading this, I'm no longer maintaining this website. Fortunately, I did everything in my power before I left to make the transfer process as simple as possible! Here is what you need to do:

1. Ensure you have an account set up on both https://gitlab.cern.ch/ and https://paas.cern.ch/

2. The current owner of the website is [Osvaldo Miguel](https://mattermost.web.cern.ch/cms-online-ops/messages/@omiguelc), reach out to him on Mattermost (@omiguelc) and he will transfer ownership of the site and its gitlab to you.

3. Follow the steps on [this page](https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookStartingGrid) to generate a `usercert` and `userkey` file. Once you have generated these files, run the command `openssl rsa -in userkey.pem -out userkey.pem` to remove the password from the key. 

4. Visit the [CERN CA Website](https://ca.cern.ch/cafiles/) and download the CERN Root CA 2 file. Rename this file to `CERN_Cert_2.crt` and place it in the same directory as the files generated in the previous step.


5. As of writing this, Osvaldo has attached my previous certificates to the site via OpenShift. If these stop working, he can teach you how to attach yours.


# Server testing/debugging

The configuration of this server is all handled using a [Docker image](https://hub.docker.com/). If you're unfamiliar, that means that it is relatively simple to reproduce an identical setup anywhere to test changes to the website before pushing them to the main, public-facing site. Unfortunately, the website is currently not functioning due to a breaking change in the `runregistry` python package, so this section is especially relevant.

The Dockerfile in this project contains complete instructions to build this server, and can be run locally to allow testing on localhost. If you would like to run locally for testing, make sure to make the following changes:

1. Before doing any of this, create a local clone of the gitlab repository to work from. This way, you can make changes without immediately enacting them on the real website.

2. Expose port 5000 instead of port 8080

3. Change entrypoint to just `["sh"]`, this will give you terminal access at which point you will have to `source startup.sh` and `python server.py` as mentioned below

4. You will need to mount your local directory (I would recommend using `~/.globus` for convenience) containing the previously set up certificates to the `/root/.globus/` directory in the docker image when you build it; Docker has documentation about how to do this, or Osvaldo can help you out.

Once the debug setup is ready, simply `source startup.sh` to declare relevant variables and `python server.py` to run the website locally. Make sure the forwarded port (5000) is open for connections, or else you won't be able to connect--this can be fixed with `firewall-cmd --add-port=5000/tcp --permanent`.

Remember, **don't push any changes made to the Dockerfile (for setting up debug mode) to the gitlab**, only changes you make to other relevant files.


# Potential next steps?

The website is now yours to run, but if you're looking for some ideas of things to improve/features to add, feel free to take a look at the `Lucas_Parting_Notes.txt` file I left in this directory.
