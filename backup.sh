#!/bin/sh
source /etc/profile
# semanage fcontext -a -t bin_t '/root/csctiming/backup.sh'
# restorecon -Fv /root/csctiming/backup.sh
# setenforce 0
export CACHE=/root/.globus/
export CACERT=/root/.globus/CERN_Cert_2.crt
export PUBLIC_KEY=/root/.globus/usercert.pem
export PRIVATE_KEY=/root/.globus/userkey.pem
export SSO_CLIENT_ID=cms-dqm-query
export SSO_CLIENT_SECRET=SVz3EZ3JJNxInqWcKOzNbKdCjBnK3iXZ
cd /root/root
. bin/thisroot.sh
. /root/csctiming/csctimingenv/bin/activate
/root/csctiming/csctimingenv/bin/gunicorn --chdir=/root/csctiming/ --workers 3 --bind 0.0.0.0:8080 wsgi:app --timeout 0
