# ENVIRONMENT
FROM gmao/centos8-gcc:10.3.0
RUN cd /etc/yum.repos.d/
RUN sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-*
RUN sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-*
RUN yum update -y
RUN dnf update
RUN dnf install git -y

# COPY
RUN mkdir /root/csctiming
COPY . /root/csctiming
WORKDIR /root/csctiming

# INSTALLATIONS
RUN mkdir -p /root/.globus /root/csctiming/tmp # create dir to mount keys and temp files
RUN source csctimingenv/bin/activate && pip install cernrequests==0.5.0 runregistry==1.3.1
RUN yum install git make cmake gcc binutils libX11-devel libXpm-devel libXft-devel libXext-devel python39 openssl-devel -y
RUN yum install epel-release -y
RUN yum install 'dnf-command(config-manager)' -y
RUN yum config-manager --set-enabled powertools
RUN yum install qt5-qtbase-devel -y
RUN yum upgrade -y
RUN yum install wget -y
RUN yum install redhat-lsb-core gcc-gfortran pcre-devel mesa-libGL-devel mesa-libGLU-devel glew-devel ftgl-devel mysql-devel fftw-devel cfitsio-devel graphviz-devel libuuid-devel avahi-compat-libdns_sd-devel openldap-devel python3-numpy libxml2-devel gsl-devel readline-devel R-devel R-Rcpp-devel R-RInside-devel wget --skip-broken -y
WORKDIR /root
RUN wget https://root.cern/download/root_v6.28.10.Linux-centos8-x86_64-gcc8.5.tar.gz	
RUN tar -xzvf root_v6.28.10.Linux-centos8-x86_64-gcc8.5.tar.gz
WORKDIR /root/csctiming

# START SERVICE
EXPOSE 8080
ENTRYPOINT ["sh", "-c", "source /root/csctiming/backup.sh"]
